# PMHTTP&#x3A; a simple HTTP server

## What is PMHTTP, exactly?

It's a static, very lightweight and in development [HTTP server](https://en.wikipedia.org/wiki/Web_server). In simple terms: if you have a static website that only contains pages and other documents ready to be served, this server may be just the thing for you.  
It's working properly (at least after many tests), but it only accepts `GET` and `HEAD` requests.  
Everything is coded in JS (no surprises here) and works with Node.js (wow, really?).  
It depends on the npm package `yargs`.  
Requests are treated asynchronously : it can handle several at once.  
About his name: it comes from the colossal lack of imagination of its author, PMLefra. It might change later (the name, not the author).  
Here's the source code on [GitLab](https://gitlab.com/PMLefra/pmhttp), and here's the [npm package page](https://www.npmjs.com/package/pmhttp).

## How do I make it work?

It's very easy, install `Node.js`, `npm`, and use the command `npm i pmhttp` or `npm i -g pmhttp` to install it globally.  
Then, localize the folder that you want to serve and memorize its path (abolute or relative).  
Input one of these commands:  

-   if installed globally: `pmhttp -t [folder to serve]`  
-   else, `node [path to the PMHTTP folder] -t [folder to serve]`.  

If you want to know more, please read the section below about the command options.

## What are its options?

There are four different options:  
`-p port`, where `port` is the TCP port on which the server will listen ; by default: 8080.  
`-a address`, where `address` is the server's IP address ; by default: localhost (127.0.0.1).  
`-t root`, where `root` is the root folder of the website, i.e. where the documents that should be served will be served. By default, the server will serve the current folder (`.`).  
`-v` (verbose): with -v enabled, you'll have much more information available in the console, like full request headers, a preview of the request bodies, and information about the incoming connections.

## If you want to test it

A `www` folder is included that contains test pages that are written in french.
If you find any bugs or vulnerabilities, please let me know! [Here's my Twitter](https://twitter.com/Lefra99M).
