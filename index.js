const net = require("net");
const fs = require("fs");
const util = require("util");
const yargs = require("yargs");

const argv = yargs.argv;
const fsstat = util.promisify(fs.stat);
const fsexists = util.promisify(fs.exists);
const fsreadFile = util.promisify(fs.readFile);
const fsreadDir = util.promisify(fs.readdir);

// process.argv - 0 : node, 1 : path
let address = argv["a"] || "localhost";
let port = argv["p"] || 8080;
let rootFolder = argv["t"] || ".";
let verbose = Boolean(argv["v"]);
rootFolder = rootFolder.replace(/\/$/, "");

let httpversion = "1.1";

const HTTPCodes = require(__dirname + "/json/codes.json");
const HTTPTypes = require(__dirname + "/json/mime.json");
const IconFolder = fs.readFileSync(__dirname + "/img/folder.png").toString('base64');
const IconFile = fs.readFileSync(__dirname + "/img/file.png").toString('base64');;

const server = net.createServer(() => {});

server.on('close', function() {
  console.log('Server closed !');
});

server.on('listening', function() {
  console.log(`-- SERVER LISTENING ON http://${address}:${port}, AT ${rootFolder} --`);
});

function generateErrorPage(errorNumber) {
  let fstr = `<!DOCTYPE html><html lang="en"><head><meta charset="utf-8"><title>${errorNumber} | PMHTTP</title></head>`;
  return fstr + `<body><h1> Error ${errorNumber}: ${HTTPCodes[errorNumber.toString()].substr(4)}</h1></body></html>`;
}

async function generateIndexPage(dirPath) {
  let httpPath = dirPath.replace(rootFolder, "");
  let fstr = `<!DOCTYPE html><html lang="en"><head><meta charset="utf-8"><title>Index of ${httpPath} | PMHTTP</title></head>`;
  fstr += `<body><h1>Index of ${httpPath}</h1><ul>`;
  let files = await fsreadDir(dirPath);
  if (httpPath != "/") fstr += `<li><img src="data:image/png;base64, ${IconFolder}" width="14">&nbsp;<a href="${(httpPath + "..").replace(/\/\//g, '/')}">../</a></li>`;
  for (let file of files) {
    if (!await fsexists(dirPath + file)) break;
    let stat = await fsstat(dirPath + file);
    let img = stat.isDirectory() ? IconFolder : IconFile;
    fstr += `<li><img src="data:image/png;base64, ${img}" width="14">`;
    fstr += `&nbsp;<a href="${(httpPath + file).replace(/\/\//g, '/')}">${file}</a></li>`;
  }
  return fstr + `</ul></body></html>`;
}

// Header should not contain 'Content-Length', 'Server', 'Date' nor 'Connection' yet
net.Socket.prototype.sendHTTP = async function(header, content, contentType, sendContent = true) {
  if (header.length > 0) header += "\n";
  header += "Server: PMHTTP";
  header += "\nConnection: close";
  header += "\nDate: " + new Date().toUTCString();

  let appTxt = ["application/javascript", "application/json", "application/xml"];
  let isText = false;
  let cl = 0;
  if (content) {
    isText = contentType.split("/")[0] == "text" || appTxt.includes(contentType);
    cl = content.length;
    cl++; // Pour le saut de ligne final
  }
  header += `\nContent-Length: ${cl}`;

  if (verbose) console.log("-- SENT TO SOCKET --");
  else console.log("<- " + header.split("\n")[0]);
  if (content && sendContent) {
    if (isText) {
      let response = header + "\n\n" + content + "\n";
      this.write(response);
      if (verbose) {
        let logResponse = header + "\n\n";
        content = content.toString();
        logResponse += (cl > 200 ? content.substr(0, 100) + "\n[...]\n" + content.substr(cl - 100) : content) + "\n";
        console.log(logResponse);
      }
    } else {
      this.write(header + "\n\n");
      this.write(content); // content est un Buffer
      this.write("\n");
      if (verbose) console.log(header + "\n\n" + "[Binary data]");
    }
  } else {
    this.write(header + "\n\n");
    if (verbose) console.log(header + "\n\n");
  }

  this.end();
}

net.Socket.prototype.sendIndex = async function(path, noContent = false) {
  let header = `HTTP/${httpversion} ${HTTPCodes["200"]}`;
  header += "\nContent-Type: text/html";
  let content = await generateIndexPage(path);
  this.sendHTTP(header, content, "text/html", !noContent);
}

net.Socket.prototype.sendError = async function(errorCode, noContent = false) {
  let header = `HTTP/${httpversion} ${HTTPCodes[errorCode.toString()]}`;
  header += "\nContent-Type: text/html";
  let content = generateErrorPage(errorCode);
  this.sendHTTP(header, content, "text/html", !noContent);
}

net.Socket.prototype.sendRedirect = async function(code, location) {
  let header = `HTTP/${httpversion} ${HTTPCodes[code.toString()]}`;
  header += `\nLocation: ${location}`;
  this.sendHTTP(header);
}

net.Socket.prototype.sendDocument = async function(path, stats, noContent = false) {
  let type = "text/plain"; // Par défaut
  let code = "200" // Par défaut

  // Extension -> type
  let regExtension = /.*(\.\w+)$/;
  if (regExtension.test(path))
    type = HTTPTypes[regExtension.exec(path)[1].toLowerCase()] || type;

  let header = `HTTP/${httpversion} :code`;

  // Last-Modified
  if (stats) {
    let mtime = stats.mtime.toUTCString();
    if (stats.lmtime && stats.lmtime == mtime) {
      code = "304";
      noContent = true;
    } else header += "\nLast-Modified: " + mtime;
  }

  header = header.replace(":code", HTTPCodes[code]);
  header += "\nContent-Type: " + type;

  let content = code == "200" ? await fsreadFile(path) : undefined;

  this.sendHTTP(header, content, type, !noContent);
}

net.Socket.prototype.responseGET = async function(path, lmtime, noContent = false) {
  if (!path || typeof path != "string") throw "Can't respond to nothing!";
  // Sécurité : empêche le client d'accéder à des fichiers extérieurs (important !)
  path = path.split('/').filter(a => (a != '.' && a != '..')).join('/');
  path = decodeURI(path);
  path = rootFolder + path;

  // Si le fichier n'existe pas
  if (!await fsexists(path)) {
    this.sendError(404, noContent);
    return;
  }

  // Redirection dans le cas d'un dossier
  let stats = await fsstat(path);
  let endsWithSlash = path[path.length - 1] == '/';
  if (stats.isDirectory() && !endsWithSlash) {
    let redirectPath = encodeURI((path += "/").replace(rootFolder, "").replace(/\/\//g, '/'));
    this.sendRedirect(302, redirectPath);
    return;
  }

  // Cas d'un dossier
  if (endsWithSlash) {
    if (await fsexists(path + "index.php")) path += "index.php";
    else if (await fsexists(path + "index.html")) path += "index.html";
    else if (await fsexists(path + "index.htm")) path += "index.htm";
    else {
      this.sendIndex(path);
      return;
    }
  }

  stats = await fsstat(path);
  stats.lmtime = lmtime;

  // Et on envoie quand tout va bien
  this.sendDocument(path, stats, noContent);
}

net.Socket.prototype.responseHEAD = async function(path, lmtime) {
  this.responseGET(path, lmtime, true);
}

server.on('connection', function(socket) {
  if (verbose) {
    console.log('\n--------------------------------------------');
    console.log('New connection, buffer size : ' + socket.bufferSize);
    let address = server.address();
    console.log(`Server is listening at ${address.address}:${address.port} (${address.family})`);
    console.log(`REMOTE Socket is listening at ${socket.remoteAddress}:${socket.remotePort} (${socket.remoteFamily})`);
    console.log('--------------------------------------------\n');
  }

  socket.setEncoding('utf8');

  socket.setTimeout(800000, function() {
    console.log('Socket timed out');
  });

  socket.on('data', function(data) {
    if (verbose) {
      console.log("-- RECEIVED FROM SOCKET --");
      console.log(data);
    } else {
      console.log("\n-> " + data.split("\n")[0]);
    }

    // Test sur la première ligne (par exemple GET / HTTP/1.1)
    let reg = /^\s*([A-z]+)\s+(\/[\w-\/\.\?=%]*)(?:\s+HTTP\/(\d\.\d))?\s*/;
    if (!reg.test(data)) {
      socket.sendError(400);
      return;
    }
    let [method, path, version] = reg.exec(data).splice(1);
    httpversion = version || "1.1";

    let reglmtime = /\n+[Ii]f-[Mm]odified-[Ss]ince:\s+(.*)/;
    let lmtime = reglmtime.test(data) ? reglmtime.exec(data)[1] : null;

    // Séparation query / path basique
    path = path.split('?')[0];

    // Test de méthodes
    switch (method.toUpperCase()) {
      case "GET":
        socket.responseGET(path, lmtime);
        break;
      case "HEAD":
        socket.responseHEAD(path, lmtime);
        break;
      default:
        socket.sendError(501);
        break;
    }
  });

  socket.on('error', function(error) {
    console.log('Error: ' + error);
  });

  socket.on('timeout', function() {
    if (verbose) console.log('Socket timed out!');
    socket.end('Timed out!');
  });

  socket.on('end', function() {
    if (verbose) console.log('\n-- Socket ended --');
  });

  socket.on('close', function(error) {
    if (verbose) {
      let bread = socket.bytesRead;
      let bwrite = socket.bytesWritten;
      console.log('Bytes read : ' + bread);
      console.log('Bytes written : ' + bwrite);
      console.log('-- Socket closed --');
    }
    if (error) {
      console.log('Socket was closed because of a transmission error');
    }
  });
});

server.listen(port, address);